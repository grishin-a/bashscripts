#!/bin/bash

if [ "$1" == "" ];then echo -e 'Error\nYou need specify LXC container name as a parameter!';exit 1;fi
#File path
path=/var/lib/lxc/$1/rootfs
#Snapshot's path
spath="/var/snapshot-$1"
#Backup's path
bpath="/var/back"
date=`date +%Y-%m-%d-%H-%M`
#How many days to keep backups
dayexp=21

#If path-thru-argument is not exist - exit with fail
if [ ! -d "$path" ]; then echo -e 'Error! Wrong backup path!\nWrong LXC container name?\n'$path;exit 1;fi
if [ "`btrfs subvolume list -s $path 2>/dev/null`" ]
then
echo "Warning: Snapshot exist, deleting snapshot..."
btrfs subvolume delete $spath
fi
case `lxc-ls -f | awk "/$1/" | awk '{print $2}'` in
    "RUNNING" )
        echo 'Container is running'
        lxc-stop -n $1 --nokill
        if [ $? -ne 0 ];then echo "Can't stop container $spath ! Exiting with fail...";exit 1;fi
        sleep 5
        btrfs subvolume snapshot $path $spath
        if [ $? -ne 0 ];then echo "Snapshot $spath was not created! Exiting with fail...";lxc-start -d -n $1;exit 1;fi
        sleep 5
        lxc-start -d -n $1
        ;;
    * )
        echo 'Container is not running'
        btrfs subvolume snapshot $path $spath
        if [ $? -ne 0 ];then echo "Snapshot $spath was not created! Exiting...";exit 1;fi
        ;;
esac

tar --numeric-owner -czvf $bpath/$1-$date.tar.gz $spath /var/lib/lxc/$1/config
find $bpath -type f -mtime +$dayexp -exec rm -rf {} \;
#Deleteing stale snapshot
btrfs subvolume delete $spath
exit 0