#!/bin/bash
#Backup files path
path="/etc/ /var/lib/lxc/*.sh /var/log"
#How many days to keep backup files
dayexp=7
#Backup path
bpath="/var/back"
date=`date +%Y-%m-%d-%H-%M`
find $bpath -type f -mtime +$dayexp -exec rm -rf {} \;
tar -cvzf $bpath/server-$date.tgz $path
exit 0
