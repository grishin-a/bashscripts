#!/bin/bash
#set -e
backup_path="/backup/"
rotate_days="7"
vm_state="on"
def_umask=`umask`
do_shutdown() {
   local vmname=$ARG
   local vmexist=`/usr/bin/virsh list --all | grep -wi ${vmname} | wc -l`
   if [ $vmexist -ne 0 ]; then
     echo "Working with VM:${vmname}"
   else
     echo "VM name:${vmname} not found!">/dev/stderr
     do_fail
   fi
   sleep 0
   local is_down=`/usr/bin/virsh list --all | grep -wi ${vmname} | grep shut | wc -l`
   local wait_time=6
   local i=0
   /usr/bin/virsh shutdown ${vmname} > /dev/null 2>&1
   if [[ $is_down -eq 1 ]]; then
     vm_state="off"
     echo "VM already is shutted down"
   else
     echo "VM is running, trying to shutdown"
   fi
   while [[ $is_down -ne 1 ]];do
      local i=$(($i+1))
      sleep 10
      local is_down=`/usr/bin/virsh list --all | grep -wi ${vmname} | grep shut | wc -l`
      echo "Waiting VM shutdown $i"
      if [[ $i -ge $wait_time ]];then echo "Error,can't shutdown VM! Waited $(($wait_time*10)) sec">/dev/stderr;do_fail;fi
   done
   echo "VM is shutted down"
}

do_backup() {
   local vmname=$ARG
   image_path=`virsh dumpxml ${vmname} | grep "source file" | sed "s/.*='//;s/'.*//"`
   image_name=`echo ${image_path} | sed "s/.*\///"`
   image_name_backup="${backup_path}/`date +%F-%H%M`.${image_name}.lzop"
   image_name_backup_xml="${backup_path}/`date +%F-%H%M`.${vmname}.xml"
   echo "Compressing ${image_path} to ${backup_path}"
   /usr/bin/lzop -f -o${image_name_backup} ${image_path}
   echo "Dumping ${vmname} config to ${image_name_backup_xml}"
   /usr/bin/virsh dumpxml ${vmname} > ${image_name_backup_xml}
   if [ -f ${image_name_backup} ] && [ -f ${image_name_backup_xml} ];then
      echo "Backup ${vmname} was successfull"
   else
      echo "Error! Backup file was not created!">/dev/stderr
      do_fail
   fi
}

do_start() {
   local vmname=$ARG
   case ${vm_state} in
     on*)
          echo "Starting VM ${vmname} after backup"
          /usr/bin/virsh start ${vmname}
          sleep 3
          local is_running=`/usr/bin/virsh list --all | grep -wi ${vmname} | grep running | wc -l`
          if [[ ! $is_running -ge 1 ]];then echo "Error! Can't start VM:${vmname}">/dev/stderr ;fi
     ;;
     off*)
          echo "VM ${vmname} was stopped before backup"
     ;;
   esac
}

do_rotate() {
   find ${backup_path} -name '*.lzop' -o -name '*.xml' -type f -mtime +${rotate_days} -exec rm -f '{}' \;
}

do_fail() {
    umask ${def_umask}
    exit 1
}

if [[ $1 == "" ]];then echo "Usage: $0 <VMname>!">/dev/stderr;do_fail;fi
umask 0007
do_rotate ${rotate_days}
mkdir -p ${backup_path}

for ARG in $*
do
   do_shutdown $ARG
   do_backup $ARG
   do_start $ARG
done

umask ${def_umask}