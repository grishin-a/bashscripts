#!/bin/bash
if (($# <= 0)); then echo -e "Usage:\nYou need to specify LXC container's name as a parameter!";exit 1;fi
#File path
path=/var/lib/lxc/$1/rootfs
#Snapshot's path
spath="/var/snapshot"
#Backup path
bpath="/var/back"
#How many days to keep backup files
dayexp=7

date=`date +%Y-%m-%d-%H-%M`
#If path-thru-argument is not exist - exit with fail
if [ ! -d "$path" ]; then echo -e 'Error! Wrong backup path!\nWrong LXC containers name?\n'$path;exit 1;fi

if [ "`btrfs subvolume list -s $path 2>/dev/null`" ]
then
echo "Warning: Snapshot exist, deleting snapshot..."
btrfs subvolume delete $spath
fi
#Dealing with MYSQL db - rewrite this with table locks in future
case "`lxc-attach -n $1 -- systemctl status mysql | grep running | wc -l`" in
    "0" )
#echo 'SQL is not running'
        btrfs subvolume snapshot $path $spath
        ;;
    * )
#echo 'SQL is running'
        lxc-attach -n $1 -- systemctl stop mysql
        btrfs subvolume snapshot $path $spath
        lxc-attach -n $1 -- systemctl start mysql
        ;;
esac

if [ $? -ne 0 ];then echo "Snapshot $spath was not created! Exiting...";exit 1;fi
tar --numeric-owner -czvf $bpath/$1-$date.tgz $spath /var/lib/lxc/$1/config
#Deleting backup files older than $dayexp
find $bpath -type f -mtime +$dayexp -exec rm -rf {} \;
#Deleteing stale snapshot
btrfs subvolume delete $spath
exit 0
