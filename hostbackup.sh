#!/bin/bash
#set -e
backup_path="/backup/host"        #путь к директории с бекапами
rotate_days="7"                   #сколько дней хранить копии
backup_cur_path="${backup_path}/`date +%F`"
old_mask=`umask`


do_rotate() {
   find ${backup_path}/* -name '*-*-*' -type d -mtime +${rotate_days} -exec rm -rf '{}' \; > /dev/null 2>&1
}

do_backup() {
   umask 0007
   mkdir -p ${backup_path}
   /usr/sbin/rear -v mkbackup
   rm -f /var/lib/rear/output/*.iso
   mv ${backup_path}/org321-7 ${backup_cur_path}
   umask ${old_mask}
}

do_rotate
do_backup